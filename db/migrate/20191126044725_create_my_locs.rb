class CreateMyLocs < ActiveRecord::Migration[6.0]
  def change
    create_table :my_locs do |t|
      t.point :location
      t.datetime :logged_at

      t.timestamps
    end
  end
end
