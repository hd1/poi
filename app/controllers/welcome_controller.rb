class WelcomeController < ApplicationController
  def index
    begin 
      lon = params[:loc].split(',')[0].to_f
      lat = params[:loc].split(',')[1].to_f
      @ret = []
      conn = PG.connect( :dbname => 'gis', :user => 'postgres', :password => '243824' )
      query = conn.prepare('query', "select st_x(st_transform(p.way, 4283)), st_y(st_transform(p.way, 4283)), p.tags as tags from planet_osm_point p where tags::json->>'name' is not null order by st_setsrid(st_point($1, $2), 4283) <-> (SELECT st_transform(way, 4283) FROM planet_osm_point limit 1) limit 10")
      # TODO how to see prepared statement before execution?
      @res = conn.exec_prepared('query', [lon, lat]) 
      @res.each { |row|
        raw_json = "{ #{row['tags'].to_s.gsub('=>', ':')} }"
        tags = JSON.parse(raw_json)
        @ret << tags['name']
      }
      conn.close
      render json: @ret.to_json
    rescue NoMethodError
      render json: { 'error': 'missing parameter loc' }, status: 500
    end
  end
end
