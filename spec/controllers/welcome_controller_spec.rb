require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe "GET #index" do
    it "returns 10 elements with parameters" do
      get :index, params: {lat: 0, lon: 0 }
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).length).to eq(10)
      JSON.parse(response.body).each { |k| 
        expect(k.include?('name')).to be_truthy 
        expect(k.include?('bearing')).to be_truthy
        expect(k.include?('distance')).to be_truthy
        expect(k.include?('latitude')).to be_truthy
        expect(k.include?('longitude')).to be_truthy
        expect(k['name']).to be_kind_of(String)
        expect(k['bearing'].to_f).to be_kind_of(Numeric)
        expect(k['distance'].to_f).to be_kind_of(Numeric)
      }
    end

    it "raises an error if params are missing" do
      get :index
      expect(response).to have_http_status 400
      j = JSON.parse(response.body)
      expect(j).to eq({"error" => "Missing (lat)itude/(lon)gitude"})
    end

    it "never returns nil" do
      params = {"lon":1, "lat":1}
      get :index, params: params
      $body = JSON.parse(response.body)
      expect($body).not_to be_nil
    end

    it "returns different results for different locations" do
      params = {"lon":1, "lat":-117}
      get :index, params: params
      $body = JSON.parse(response.body)

      params = {"lon":80, "lat":1}
      get :index, params: params
      $body1 = JSON.parse(response.body)
      expect($body).not_to be_eql($body1)
    end

  end

end
