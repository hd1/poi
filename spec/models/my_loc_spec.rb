require 'rails_helper'
require "activerecord-postgis-adapter"

RSpec.describe MyLoc, type: :model do
  context 'Validate column constraints' do
    it 'has a timestamp less than now' do
      d = Time.current
      MyLoc.all.each { |e| expect(d > e.logged_at) }
    end
    
    it 'verifies that all locations have valid longitudes' do
      MyLoc.all.each { |e| expect(Math.sqrt(e.location.lng.to_f**2) <= 90) }
    end

    it 'verifies that all locations have valid latitudes' do
      MyLoc.all.each { |e| expect(Math.sqrt(e.location.lat.to_f**2) <= 180) }
    end
  end
end
